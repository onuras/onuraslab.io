#!/usr/bin/env python

import os
from pathlib import Path
from PIL import Image, ImageOps


THUMBNAIL_SIZE = (300, 200)
BIG_IMAGE_SIZE = (1280, 960)
QUALITY = 90


def resize(input_path, output_path, size):
    image = Image.open(input_path)
    image = ImageOps.exif_transpose(image)
    image.thumbnail(size)
    image.save(output_path, quality=QUALITY)


if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    for root, dirs, files in Path('content/images').walk(on_error=print):
        for file in files:
            image_path = root.joinpath(file)

            if image_path.suffix != '.jpg' and image_path.suffix != '.png':
                print('Skipping file:', image_path)
                continue

            if any(ele in file for ele in ['t.jpg', 'b.jpg', 't.png', 'b.png']):
                continue

            thumbnail_path = root.joinpath(image_path.stem + 't' + image_path.suffix)
            big_image_path = root.joinpath(image_path.stem + 'b' + image_path.suffix)

            if not thumbnail_path.exists():
                resize(image_path, thumbnail_path, THUMBNAIL_SIZE)
            if not big_image_path.exists():
                resize(image_path, big_image_path, BIG_IMAGE_SIZE)
