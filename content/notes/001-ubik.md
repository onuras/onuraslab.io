Title: Ubik
Date: 2007-08-27
Thumbnail: https://upload.wikimedia.org/wikipedia/en/a/af/Ubik%281stEd%29.jpg
Lang: tr

Sevgili arkadaşım Murat'ın tavsiyesi üzerine aldığım, 1969'da
[Philip K. Dick](http://en.wikipedia.org/wiki/Philip_K._Dick)
tarafından yazılmış bir bilim kurgu romanı
[Ubik](http://en.wikipedia.org/wiki/Ubik).

Bir sağduyu organizasyonunda görev yapan Joe Chip etrafında gelişen
olayları konu almaktadır. Sağduyu organizasyonu psijik güçleri
durdurma görevi yapmaktadır ve bir gün organizasyon başkanı Glen
Runcinter ve 11 çalışanı bir gücü engellemek için Ay'a giderler. Bir
kaza olur. Aralarından biri için yaşam kısıtlanmıştır, yoksa
diğerleri için mi?

Arka kapak: <i>Bilimkurgunun büyük ustası Philip K. Dick'ten yaşam,
ölüm ve gerçeklik üstüne bir başyapıt daha...<br /><br />Ben Ubik'im.
Evrenden önce ben vardım. Güneşleri ben yarattım. Yaşamları ve
yaşanacak yerleri ben yarattım; onları buraya getirdim ve onları
oraya koydum. Benim istediğim gibi davranırlar, ben ne dersem
onu yaparlar. Benim sözüm ve adım asla söylenmedi, kimse bilmez
benim adımı. Bana Ubik diyorlar, ama adım bu değil. Ben varım. Her
zaman var olacağım.</i><br /><br />Mutlaka okunması gereken bir
kitap olarak düşünüyorum ve şimdiye kadar okuduğum favori
kitabım olmayı başardı.
