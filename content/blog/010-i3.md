Title: i3
Subtitle: Dinamik döşeme pencere yöneticisi
Date: 2014-02-24
Cover: /images/i3/cover.jpg
Lang: tr


[i3](http://i3wm.org/) modern bir döşeme pencere yöneticisidir (tiling
window manager). Başıma gelen en güzel şey ne diye sorsalar, i3 derim heralde.
Peki nedir i3'ü benim için bu kadar çekici kılan ona bakalım.

i3 kullandığım ilk tiling window manager. Gelişmiş kullanıcılar (power users)
için hazırlanmış bu pencere yöneticisi, pencereleri ekranınızda hiç boşluk
kalmayacak şekilde döşüyor. Bu da size, özellikle her işinizi klavyeden
yapmaya alışkınsanız inanılmaz bir verimlilik sağlıyor.

Gündelik kullandığım örnek bir masaüstüm aşağıda yer alıyor:

[![i3 screenshot 01]({static}/images/i3/01_small.jpg)]({static}/images/i3/01_orig.jpg)

Gördüğünüz gibi i3 monitörümdeki tüm alanı en verimli şekilde kullanmamı
sağlıyor. Herşey klavyeden kontrol edildiğinden (isterseniz mouse ile de
kontrol edebilirsiniz); başlık çubuğu ve pencere yönetim düğmeleri (simge
durumuna getirme, tam ekran vs.) bulunmamakta.

i3 pencereleri yatay ve dikey döşemenin yanında, yığın ve sekme olarak da
döşeyebiliyor. Bu da size iç içe geçmiş kullanılabilir sınırsız sayıda pencere
oluşturabilme imkânı tanıyor.

[![i3 screenshot 02]({static}/images/i3/02_small.jpg)]({static}/images/i3/02_orig.jpg)

Yukarıdaki örnek çalışma alanında görebildiğiniz gibi, pencereleri
istediğiniz gibi açalabilirsiniz. Sağ tarafta 2 sekme altında açılmış
3 sekme ve 3. sekmede açılmış yine onlarca terminal penceresi yer alıyor.
Bu resmi sekmeli kısımdan ikiye ayırıp bakacak olursak, sağ tarafta aslında
yepyeni bir masaüstü kullandığımızı bile düşünebiliriz. Pencereleri nasıl
döşeyeceğinize karar vermek hayal gücünüze kalmış.

i3 aynı zamanda sınırsız sayıda çalışma ortamı (workspace) oluşturmaya da
imkân tanıyor. Yeni çalışma ortamı geçis yaptığınızda oluşturuluyor ve eğer
içerisinde bir pencere varsa hayatta kalıyor.

i3 pencere eylemlerini gerçekleştirmek için JSON tabanlı IPC arabirimi
kullanıyor. Yani bir örnek ile açıklamak gerekirse, siz bir pencereyi sola 
taşımak için (benim kullandığım) `$mod+Shift+j` tuş kombinasyonunu
kullandığınızda aslında i3 pencere yöneticisine `move left` mesajını göndermiş
oluyorsunuz. Bu yapı da size inanılmaz bir özelleştirme imkanı tanıyor.
Pencere yöneticinizi istediğiniz programlama diliyle istediğiniz gibi kontrol
edebilirsiniz.

i3 aynı zamanda vi gibi farklı modları da destekliyor. Örneğin vi de komut
modundayken tuşlar farklı eylemler gerçekleştirirken INSERT veya VISUAL
modlarında farklı eylemler gerçekleştiriyor. Aynı şey i3 için de sağlanmış.
Normalde pencereler arası geçis yapmak için kullandığınız HJKL tuş takımını,
`$mod+r` tuşlarına bastığınızda `resize` moduna girerek
pencereleri yeniden boyutlandırmak için kullanabilirsiniz. Üstelik bu
özelliklerin hepsi özelleştirilebilir, istediğiniz kadar farklı mod
tanımlayabilir, istediğiniz tuşlarla farklı eylemleri gerçekleştirebilirsiniz.
Ve tüm bu eylemler inanılmaz hızlı gerçekleşiyor. i3'ün şuanki sürümü 859kb
ve gücünü C'den alıyor. Şuan çalışan masaüstü oturumum ise sadece 27mb bellek
kullanıyor.

Durum çubuğu (`i3status`) da kendisi gibi verimli ve istediğiniz programlama
dilini kullanarak istediğiniz modülü yazmanıza olanak tanıyor.

Özelleştirilmiş bir durum çubuğunu aşağıdaki ekran görüntüsünde görebilirsiniz:

[![i3 screenshot 05]({static}/images/i3/05_small.jpg)]({static}/images/i3/05_orig.jpg)

Fare kullanmaya alışkınsanız, i3 pencereleri fare yardımıyla da daha verimli
kullanmanız mümkün. Örneğin standart bir pencere yöneticisinde bir pencereye
odaklanmak için, pencereye tıklamanız gerekirken, i3'de tüm pencereler zaten
önde olduğundan, imleci pencerenin üzerine getirdiğinizde pencere aktif hale
geliyor. Yine de eğer her işinizi fare ile yapmaya alıştıysanız i3 sizin
için uygun olmayabilir.

Eğer her işinizi terminalden yapıyor ve terminalinizde screen veya tmux
kullanmaya alışkınsanız, i3'ü çok gelişmiş bir terminal çoklayıcı olarak
kullanabilirsiniz. Haydi ne bekliyorsunuz, hemen favori paket yöneticiniz ile
i3 denemeye başlayın :) Tüm sorularınızı ayrıntılı cevaplandıran kullanıcı
rehberine [buradan](http://i3wm.org/docs/userguide.html) ulaşabilirsiniz.

Son olarak bir kaç ekran görüntüsü daha:

[![i3 screenshot 03]({static}/images/i3/03_small.jpg)]({static}/images/i3/03_orig.jpg)

Yazıyı hazırlarken chrome'a ihtiyaç duymadığımdan, hemen chrome penceresini
sekmeler haline dönüştürüp, yeni bir sekmede vim ile bu yazıyı hazırladım.
Diğer pencereler ise sürekli kontrol ettiğim pencereler olduğundan hepsi görüş
alanım içerisinde.

Örnek bir geliştirme ortamı:

[![i3 screenshot 04]({static}/images/i3/04_small.jpg)]({static}/images/i3/04_orig.jpg)

Masaüstünüzün büyük bir bölümüyle kod geliştirken, hemen sağda test edebilir
git işlemlerinizi gerçekleştirebilirsiniz. Bu sadece çok basit bir örnek (git
blame kullanımı çok yersiz olmuş farkındayım), daha
önce de dediğim gibi i3 her türlü kullanım alanında size verimlilik
sağlayacaktır.
