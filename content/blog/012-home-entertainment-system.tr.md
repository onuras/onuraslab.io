Title: Ev Sunucusu ve Eğlence Sistemi
Date: 2024-09-06T15:44:53+03:00
Lang: tr
Gallery: true
Status: draft


[TOC]

## Donanım

### NAS

1.  **Topton J6413 NAS motherboard**: Intel Celeron J6413 işlemci ile gelen
    çok tatlı düşük güç tüketen bir Mini ITX anakart. Bu Türkiye'de
    alabileceğim tek Toptop anakarttı. Şimdi daha çok seçenek mevcut ve
    benzer bir sistem topluyorsanız N100 modellerini tercih edin. Hem daha
    yeni jenerasyon işlemci, hem de 10Gb ethernet ile geliyolar. Bu anakartla
    gelen işlemci benim ihtiyaçlarım için yeterli. Anakartın bazı özellikleri:

    1. Intel Celeron J6413 işlemci (4 Core, 3.0 GHz maksimum boost hızı).
    2. 2x PCIe 3.0 M.2 NVMe slotu.
    3. 6x SATA 3.0 portu, JMB585 kontrolcüsüyle birlikte.
    4. 4x 10Gb Ethernet.

    Tüm özelliklerini görmek için <a href="https://ae01.alicdn.com/kf/S20ccf8787fea4247bdc69adc897a2ba82.jpg" target="_blank">buraya</a> tıklayabilirsiniz.

2. **2x 32 GB Kingson 3200MHz SO-DIMM DDR4 bellek**: Anakartın desteklediği
    maksimum bu kadar.

3. **2x 1TB Kioxia Exceria Plus G3 NVMe SSD disk**: Anakart sadece PCIe 3.0
    desteklediği için bu diskler biraz fazla, fakat bu diskleri çok uygun
    fiyata almıştım. RAID1 konfigürasyonunda boot/işletim sistemi diskleri
    olarak kullanılıyorlar.

4. **6x 22TB Exos X22 HDD**: NAS'lar için biçilmiş kaftan CMR hard diskler.

5. **Fractal Node 304 Case**: Bu kasayı ne kadar sevdiğimi anlatamam. Eğer
    evinizde alan sorununuz varsa, bence evde NAS yapmak için en iyi tercih.
    Micro DTX anakartlara kadar destekliyor ve 6x 3.5" HDD takabiliyorsunuz. Boyutu
    oldukça küçük, internette bir çok modu da mevcut.

6. **Thermaltake PSU**: Daha önceki PC'mde kullandığım rastgele bir PSU.

<div class="gallery" markdown="span">
[![Seagate Exos X22]({static}/images/home-entertainment-system/20240907_171144t.jpg)]({static}/images/home-entertainment-system/20240907_171144b.jpg)
[![Tatlı Anakart]({static}/images/home-entertainment-system/20240907_112950t.jpg)]({static}/images/home-entertainment-system/20240907_112950b.jpg)
[![Fractal Node 304 NAS]({static}/images/home-entertainment-system/20240907_181854t.jpg)]({static}/images/home-entertainment-system/20240907_181854b.jpg)
</div>

### Oyun Makinesi

Atari 2600 döneminden buyana sürekli oyun oynayan biriyim. Bu kurulumda oyun
makinesi de benim için çok önemli. Oyunları TV başında oynamak benim için
hep keyif olmuştur. Sistem emulasyon oyunlarını ve en sevdiğim
oyunlardan Nioh 2/PoE/Diablo 2 Ressurrected gibi oyunları 4K 60 FPS sorunsuz
oynatabiliyor.

1. **JGINYUE B760i motherboard**: Mini ITX anakartlar çok pahalı ve Türkiye'de
    çok fazla seçeneğimiz yok. Bu ucuz bir anakart ve Çin malı anakartları
    denemek için aldım. Dezavantajlarını iyi araştırıp ona göre alırsanız,
    benim gibi sorunsuz kullanabilirsiniz.
2. **Intel Core i5-13600KF**: Bu sistem için biraz fazla olan bir işlemci.
    Yüksek TDP ve enerji tüketimi ısı olarak geri dönüyor ve bu Mini ITX
    kurulumlarında isteyeceğimiz son şey. 13400 (ya da 7650X3D) daha
    uygun olur ama almış olduğum bu işlemciyi indirimde yakalamıştım ve
    kutusuz (tray) olarak aldığım için 13400'den çok daha ucuzdu.
3. **GIGABYTE RTX 4060 OC LP**: Low profile 2 slotlu ekran kartı, bu kurulumun
    en önemli parçası.
4. **Kingston KVR32N22S8/16 16GB (2x16GB)**: Günümüz oyunları için yeterli RAM.
5. **DeepCool AN600**: Anakartımıza uygun işlemci soğutucu.
6. **2x Kioxia Exceria Pro 1 TB**: Aslında bir tane yeterli olur, Mini ITX
    sistemleri sökme takma problem olduğu için anakartın arkasındaki yuva
    boş kalmasın diye 2 tane aldım.
7. **Lzmod A24-V5 Case**: Neredeyse RXT 4060 low profile ekran kartı için
    tasarlanmış çok tatlı bir kasa. Kalitesi de beklentimin çok üzerinde çıktı.
8. **Silverstone SST-FX500-G**: Türkiye'den alabileceğim tek Flex güç kaynağı
    buydu. Flex sunucularda kullanılan bir form yapısı olduğu için, üzerinde
    bulunan fan da sunucu fanı. İnanılmaz bir gürültü yapıyor. Bu tür Mini ITX
    ev oyun sistemlerinde standart Enhance 7660b güç kaynağıdır.
    Ulaşabiliyorsanız onu tercih edebilirsiniz.
9. **Intel AX201NGW**: Anakart herhangi bir CNVi modülü ile gelmediğinden
    Wi-Fi 6/Bluetooth CNVi modülü.
10. **Xbox Oyun Kumandası (9. nesil)**: Windows ile tam uyumlu dertsiz
    tasasız kullanabileceğiniz bluetooth üzerinden kablosuz çalışan oyun
    kumandası.
11. **8BitDo Sn30 Pro**: Hem laptop taşırken yanımdan ayırmadığım hem de ikinci
    oyuncu kumandası olarak kullandığım, SNES kumandasından esinlenmiş oyun
    kumandası. Bir hayli kaliteli.

<div class="gallery" markdown="span">
[![Mini ITX Bileşenler]({static}/images/home-entertainment-system/IMG_20240313_181058t.jpg)]({static}/images/home-entertainment-system/IMG_20240313_181058b.jpg)
[![Mini ITX Gaming Setup]({static}/images/home-entertainment-system/IMG_20240314_033624t.jpg)]({static}/images/home-entertainment-system/IMG_20240314_033624b.jpg)
[![Roman Boyutunda PC Hazır]({static}/images/home-entertainment-system/IMG_20240314_034005t.jpg)]({static}/images/home-entertainment-system/IMG_20240314_034005b.jpg)
</div>


### TV/BOX

2010 dan beri tüm görsel medya tüketimimi **65" LG OLED CX** TV'de yapıyorum.
Hâlâ günümüzde çağının çok ilerisinde ve bu televizyonda Dolby Vision içerik
izlemek acayip büyük bir keyif. Yeni TV alacaklar LG OLED evo G4 düşünebilir.
Fakat uyarayım, bir kere bu TV'de bir şey izleyince artık başka ekrana bakamaz
oluyorsunuz.

Kullandığım televizyonu ne kadar sevsem de, LG'nin televizyonlarında kullandığı
işletim sistemi *LG webOS* için aynı şeyleri söyleyemeyeceğim. Özellikle
lisans nedeniyle DTS desteğinin olmaması (Dolby'nin Dobly Atmos dayatması
nedeniyle), PSG altyazıları desteklememesi, Dolby Vision'ı MKV containerlarında
oynatamaması ve en önemlisi Kodi'nin native olarak çalışmaması nedeniyle
bir Android/Google TV Box ihtiyacı karşıma çıktı.

Bunun için tercihim **Xiaomi TV Box S 2nd Gen** oldu. Aslında bu işin
standartı Nvidia Shield, fakat Türkiye'de inanılmaz faiş bir fiyata
satılıyor. Xiaomi'de yukarıda saydığım ve LG'nin yapamadığı her şeyi sorunsuz
yapıyor.


### Sunucu

**Kimsufi KS-4**: OVH'nin düşük bütçeli dedike sunucusu ayda $18'a şu
özelliklerle geliyor:

* Xeon E3-1230v6 4 core 8 thread işlemci.
* 16 GB RAM.
* 2x 2TB HDD.
* 300 Mbps sınırsız bağlantı.

Bu sunucu *seedbox* ve bir kaç kişiye verdiğim *jellyfin* için kullanılıyor.



## Yazılım

İşletim sistemi olarak tüm makinelerde (oyun makinesi hariç) elbette Debian
çalışıyor. Onun dışında tüm servisler docker container'ı olarak Ansible ile
otomatik deploy edilmiş durumda.


### ZFS

*Milyar dolarlık dosya sistemi*, Oracle'ın CDDL lisansı nedeniyle hiç bir zaman
kernel'e giremeyecek olsa da openzfs ile Linux'de kullanılabiliyor. ZFS aynı
zamanda bir volume yöneticisi'de olduğu için, partitionlardan ziyade en
iyi direkt disklerle çalışır.

ZFS pool'unu oluştururken kullandığımız seçeneklere bakalım:

```sh
zpool create \
    -o ashift=12 \
    -O mountpoint=/mnt/archive \
    -O recordsize=1M \
    -O atime=off \
    -O compression=lz4 \
    -O xattr=sa \
    raidz2 \
    ata-ST22000NM001E-3HM103_ZX22XXX \
    ata-ST22000NM001E-3HM103_ZX26XXX \
    ata-ST22000NM001E-3HM103_ZX21XXX \
    ata-ST22000NM001E-3HM103_ZX22XXX \
    ata-ST22000NM001E-3HM103_ZX26XXX \
    ata-ST22000NM001E-3HM103_ZX26XXX
```

* **ashift=12**: Her aygıt için blok tahsis boyutunu (logical block allocation
    size) belirler. Modern HDD'lerde bu boyut 4096 byte'dır. Benim kullandığım
    Bu değer `smarctl -a /dev/sda` komutu ile görülebilir. ashift değeri 2^12
    yani 4096'a denk gelmektedir.

* **mountpoint=/mnt/archive**: Dosya sisteminin mount edileceği yol.

* **recordsize=1M**: Her bir ZFS dataset'i için maksimum mantıksal blok boyutunu
    belirler. ZFS'de varsayılan recordsize=128K'dır. Benim bu diskte
    depolayacağım dosyaların neredeyse tamamı çok büyük boyutlu dosyalar olacağı
    için, IO operasyonlarını azaltmak adına 1M seçtim.

* **atime=off**: Dosya access time güncellemelerini kapatır. Bu IO'u bir hayli
    düşüren bir seçenek. Ben her sistemde `atime`'ı kapatırım. Alternatif olarak
    `relatime` da düşünülebilir.

* **compression=lz4**: ZFS sıkıştırma özelliğini etkinleştirir ve sıkıştırma
    algoritması olarak `lz4` kullanır. ZFS'in COW (copy on write) doğası gereği
    her bir mantıksal blok'u sıkıştırabilir. Eğer bir dosya zaten
    sıkıştırıldıysa (encode edilmiş video, arşiv dosyaları vs.), sıkıştırılma
    yapılmadan kullanılır.

* **xattr=sa**: Linux Extended Attribute'leri gizli dizinler yerine inodelarda
    saklamaya yarar.

* **raidz2**: ZFS'in sunmuş olduğu RAID 6 benzeri RAID özelliği. Bu
    parametreden sonra eklenen aygıtların RAID-Z2 ile yapılandırılacağını
    belirtir. Aygıt havuzunda 2 disk parity olarak kullanılır. Array, aynı
    anda 2 disk bozulsa bile hiç bir sorun olmadan okuma ve yazma faaliyetlerini
    sürdürebilir.

* **ata-ST22000NM001E-3HM103_ZX26XXX**: Array'e eklenecek disk idleri. Linux
    altında `/dev/sdX` gibi disk yolları değişken olduğu için, benimki gibi
    küçük kurulumlarda `/dev/disk/by-id/` altındaki disk idlerinin kullanılması
    [önerilir](https://openzfs.github.io/openzfs-docs/Project%20and%20Community/FAQ.html#selecting-dev-names-when-creating-a-pool-linux).
    Bu ileride bozulan diskin kolayca tespit edilip değiştirilebilmesini
    sağlayacak.


### Servisler

Kendi host ettigim ve kullandığım tüm servisler Docker containerları olarak
çalışıyor. Kubernetes bu ortam için aşırıya kaçmak olacağından, servisleri
docker compose ile deploy ediyorum.


#### Monitoring

* [Prometheus](https://prometheus.io/): Çalışan servislerin metriklerinin
    toplanması ve izlenmesi.
* [AlertManager](https://prometheus.io/docs/alerting/latest/alertmanager/):
    Toplanan metriklerden alarm verilmesini saglıyor. Alarmları oluşturduğum
    bir telegram kanalına bağladım. Bu sayede her an her durumdan haberdar
    olabiliyorum.
* [Grafana](https://grafana.com/): Metriklerin görsel edilmesi ve
    görselleştirilmesini sağlıyor. Çalışan tüm sistem ve servislerin izlenmesini
    bu arayüzden yapıyorum.
* [Loki](https://grafana.com/oss/loki/): Çalışan tüm servislerin loglarının
    toplandığı aggregation sistemi. Belirli bir zaman tüm logları tutup sonra
    otomatikmen siliyor. Belirlenen kurallara göre de alarm verebiliyor. Docker
    driver'ı ile de docker ile tam uyumlu çalışabiliyor.
* Kullanılan exporterlar:
    * [node-exporter](https://github.com/prometheus/node_exporter): Donanım
        ve işletim sistemi düzeyinde bir çok metrik sağlıyor.
    * [blackbox-exporter](https://github.com/prometheus/blackbox_exporter):
        VPN bağlantılarını, makinenin internet ping sürelerini, servislerin
        sağlık durumlarını vs izlemek için kullandığım exporter.
    * [smartctl-exporter](https://github.com/prometheus-community/smartctl_exporter):
        NAS içerisinde bulunan HDD'lerin smartctl istatistiklerini toplayan
        exporter.
    * [qbittorrent-exporter](https://github.com/martabal/qbittorrent-exporter):
        Seedbox, NAS ve OVH'den tüm qbittorrent verilerinin toplanmasını
        sağlayan exporter.


#### Yazılım Geliştirme

* [Gitea](https://about.gitea.com/): Go ile yazılmış, çok az sistem kaynağı
    tüketen GitHub ve GitLab benzeri bir proje/kaynak kod host servisi. Açık
    kaynak olmayan tüm özel kodlarımı burada geliştiriyorum.
* [Woodpecker CI](https://woodpecker-ci.org/): Gitea GitHub actions benzeri
    bir CI sistemi sağlıyor. Fakat bu özellik geçenlerde eklendi ve
    ben çok eskiden beri Woodpecker kullanıyorum. Bir hayli hızlı ve sistem
    kaynağı tüketmiyor.


#### Haber/Video

* [FreshRSS](https://freshrss.org/): RSS ve Atom feed okuyucusu.
    Gündemi, teknoloji haberlerini ve bazı blogları buradan takip ediyorum.
* [libreddit](https://github.com/libreddit/libreddit): Web tabanlı bir reddit
    arayüzü.
* [Invidious](https://invidious.io/): Youtube'un algoritması
    içerisinde kaybolmadan, sadece izlemek istediğiniz şeye yoğunlaşmanızı
    sağlayan bir youtube arabirimi.
* [ozgursozluk](https://github.com/beucismis/ozgursozluk): Ekşisözlük arabirimi.
* [stash](https://github.com/stashapp/stash)


Onur

- [ ] Test
- [X] Test


<!--
| IPv4         | Name                        | Ingress                        |
|--------------|-----------------------------|--------------------------------|
| 172.16.31.10 | grafana                     | grafana.vpn.onur.im            |
| 172.16.31.11 | loki                        | loki.vpn.onur.im               |
| 172.16.31.12 | node-exporter               |                                |
| 172.16.31.13 | blackbox-exporter           |                                |
| 172.16.31.14 | prometheus                  | prometheus-atlas.vpn.onur.im   |
| 172.16.31.15 | alertmanager                | alertmanager-atlas.vpn.onur.im |
| 172.16.31.16 | smartctl-exporter           |                                |
| 172.16.31.20 | gitea                       | git.vpn.onur.im                |
| 172.16.31.21 | gitea-db                    |                                |
| 172.16.31.22 | gitea-woodpecker-server     | ci.vpn.onur.im                 |
| 172.16.31.23 | gitea-woodpecker-agent      |                                |
| 172.16.31.25 | postfixrelay                |                                |
| 172.16.31.26 | acme.sh                     |                                |
| 172.16.31.31 | stash                       | stash.vpn.onur.im              |
| 172.16.31.40 | qbittorrent                 | torrent.vpn.onur.im            |
| 172.16.31.41 | qbittorrent-exporter-atlas  |                                |
| 172.16.31.42 | qbittorrent-exporter-apollo |                                |
| 172.16.31.43 | qbittorrent-exporter-whale  |                                |
| 172.16.31.45 | samba                       |                                |
| 172.16.31.50 | invidious                   | youtube.vpn.onur.im            |
| 172.16.31.51 | invidious-db                |                                |
| 172.16.31.60 | freshrss                    | reader.vpn.onur.im             |
| 172.16.31.67 | znc                         |                                |
| 172.16.31.70 | searxng                     | search.vpn.onur.im             |
| 172.16.31.71 | searxng-reddit              | search.vpn.onur.im             |
| 172.16.31.80 | nginx                       |                                |
| 172.16.31.90 | reddit                      | reddit.vpn.onur.im             |
| 172.16.31.91 | ozgursozluk                 | eksi.vpn.onur.im               |
-->
